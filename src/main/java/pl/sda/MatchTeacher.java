package pl.sda;

import org.springframework.stereotype.Component;

@Component
public class MatchTeacher implements Teacher {

    private final MottoService mottoService;

    public MatchTeacher(MottoService mottoService) {
        this.mottoService = mottoService;
    }

    public String greet() {
        return "Hi, I'm math teacher";
    }

    public String motto() {
        return mottoService.sayMotto();
    }
}
