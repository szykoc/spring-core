package pl.sda;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ComponentScan
public class AppConfig {

    @Bean
    HistoryTeacher historyTeacher() {
        return new HistoryTeacher(happyMottoService());
    }

    @Bean
    MatchTeacher matchTeacher() {
        return new MatchTeacher(sadMottoService());
    }

    @Bean
    SadMottoService sadMottoService() {
        return new SadMottoService();
    }

    @Bean
    HappyMottoService happyMottoService() {
        return new HappyMottoService();
    }
}
