package pl.sda;

import org.springframework.stereotype.Component;

@Component
public class HistoryTeacher implements Teacher {

    private final HappyMottoService happyMottoService;

    public HistoryTeacher(HappyMottoService happyMottoService) {
        this.happyMottoService = happyMottoService;
    }

    public String greet() {
        return "Hi, I'm history teacher";
    }

    public String motto() {
        return happyMottoService.sayMotto();
    }
}
