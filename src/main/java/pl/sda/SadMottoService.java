package pl.sda;

import org.springframework.stereotype.Component;

@Component
public class SadMottoService implements MottoService {
    public String sayMotto() {
        return "Its not your day";
    }
}
