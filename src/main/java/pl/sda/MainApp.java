package pl.sda;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainApp {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);

        MatchTeacher matchTeacher = context.getBean("matchTeacher", MatchTeacher.class);
        HistoryTeacher historyTeacher = context.getBean("historyTeacher", HistoryTeacher.class);

        System.out.println(matchTeacher.greet());
        System.out.println(matchTeacher.motto());

        System.out.println(historyTeacher.greet());
        System.out.println(historyTeacher.motto());



    }
}
