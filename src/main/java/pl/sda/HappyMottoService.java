package pl.sda;

import org.springframework.stereotype.Component;

@Component
public class HappyMottoService implements MottoService {
    public String sayMotto() {
        return "Today will be good day";
    }
}
