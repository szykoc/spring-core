package pl.sda;

public interface Teacher {
    String greet();
    String motto();
}
